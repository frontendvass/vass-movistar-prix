.modal {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
    //display: none;
    overflow: hidden;
    outline: 0;
    &--open {
        overflow: hidden;
        .modal {
           overflow-x: hidden;
           overflow-y: auto;
        } 
    }
    &--dialog {
        position: relative;
        width: auto;
        margin: .5rem .9375rem;
        pointer-events: none;
        &.modal {
            &--xs {
                max-width: 290px;
                margin: 1.75rem auto;
            }
        }
        &.modal--center {
            top: 50%;
            transform: translate(0 , -50%);
            left: 0;
            right: 0;
            margin: auto;
        }
    }
    &--content {
        position: relative;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid rgba(0,0,0,.2);
        border-radius: .3rem;
        outline: 0;
    }
    &--head {
        display: flex;
        align-items: flex-start;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        padding: 1rem;
        border-bottom: 1px solid transparent;
        border-top-left-radius: .3rem;
        border-top-right-radius: .3rem;
        .modal--icon {
            display: block;
            margin: -48px auto 0;
        }
        .modal--close {
            position: absolute;
            right: 0;
            top: -35px;
            padding: 0;
            background-color: transparent;
            border: 0;
            -webkit-appearance: none;
        }
        .titulo {
            display: block;
            color: $dark;
            text-align: center;
            font-weight: normal;
            margin-bottom: 0;
            width: 100%;
        }
    }
    &--body {
        position: relative;
        flex: 1 1 auto;
        padding-top: .75rem;
        padding-bottom: .75rem;
        padding-left: .75rem;
        padding-right: .75rem;
        .titulo {
            color: $dark;
            text-align: center;
            font-weight: normal;
            margin-bottom: 1rem;
        }
        .modal {
            &--list {
                &-radio {
                    margin-bottom: 2.4rem;
                    .form--radio {
                        margin-bottom: 0;
                        border-bottom: 1px solid $very-light-pink;
                        label {
                            padding-top: 1rem;
                            padding-bottom: 1rem;
                        }
                    }
                }
            }
            &--scroll {
                position: relative;
                &::after {
                    content: "";
                    position: absolute;
                    left: 0;
                    right: 0;
                    bottom: -1px;
                    width: 100%;
                    height: 40px;
                    background-image: linear-gradient(180deg,rgba(255,0,0,0),#fff);
                }
                .scroll-content {
                    overflow-y: scroll;
                    height: 450px;
                    &.scroll-xs{
                        height: 200px;
                    }
                    &.scroll-sm{
                        height: 300px;
                    }
                    > .titulo {
                        color: $dark;
                        text-align: center;
                        font-weight: normal;
                        margin-bottom: 1rem;
                    }
                }
            }
        }
    }
    &--footer {
        display: flex;
        align-items: center;
        padding: .75rem 1rem;
        border-top: 1px solid transparent;
        .modal--buttons {
            width: 100%;
            .btn{
                display: block;
                width: 100%;
                margin: auto;
                margin-bottom: .25rem;
                font-size: 14px;
                &:last-child {
                    margin-bottom: 0;
                }
            }
        }
    }
}
.backdrop{
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 9;
}