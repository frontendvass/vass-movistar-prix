# Front end Core

Core with the necessary file to start a front end project.

Run 'npm install -d'

Run 'gulp' to start the server and the watcher

Run 'gulp images' to compress the images

Run 'gulp fonts' to copy the fonts

Create a new repository on the command line

git init

git add README.md

git commit -m "first commit"

git remote add origin git@github.com:Name/Name.git

git push -u origin master